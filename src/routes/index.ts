import { app } from '../express'
import { userRouter } from './user'
import { filmRouter } from './films'
import { voteRouter } from './vote'
app.use('/user', userRouter)
app.use('/film', filmRouter)
app.use('/vote', voteRouter)