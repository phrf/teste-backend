import { Router } from 'express'
import { getAuthMiddleware, getUserController, getUserMiddlewareDataValidator } from '../../utils/factory_classes'

const userRouter = Router()
const auth = getAuthMiddleware()
const dataValidator = getUserMiddlewareDataValidator()
const userController = getUserController()

userRouter.post(
    '/new',
    (req, res, next) => dataValidator.validateCreation(req, res, next),
    (req, res) => userController.create(req, res)
)

userRouter.put(
    '/edit/:id',
    (req, res, next) => auth.checkToken(req, res, next),
    (req, res, next) => dataValidator.validateEdition(req, res, next),
    (req, res) => userController.edit(req, res)
)

userRouter.delete(
    '/delete/:id',
    (req, res, next) => auth.checkToken(req, res, next),
    (req, res) => userController.delete(req, res)
)

export { userRouter }