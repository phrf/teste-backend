import { Router } from 'express'
import { VoteMiddlewareDataValidator } from '../../middlewares/validators/vote.middleware'
import { getAuthMiddleware, getFilmMiddleware, getUserService, getVoteController } from '../../utils/factory_classes'

const voteRouter = Router()
const auth = getAuthMiddleware()
const userService = getUserService()
const filmMiddleware = getFilmMiddleware()
const voteController = getVoteController()
const dataValidator = new VoteMiddlewareDataValidator()
voteRouter.post('/:filmId/:userId',
    (req, res, next) => auth.checkToken(req, res, next),
    (req, res, next) => filmMiddleware.filmExists(req, res, next),
    (req, res, next) => auth.checkUserPermission(req, res, next, userService, false),
    (req, res, next) => dataValidator.validateCreation(req, res, next),
    (req, res) => voteController.create(req, res)
)

export { voteRouter }