import { Router } from 'express'
import { getAuthMiddleware, getFilmController, getFilmMiddlewareDataValidator, getUserService, getVoteController } from '../../utils/factory_classes'

const filmRouter = Router()
const auth = getAuthMiddleware()
const userService = getUserService()
const filmController = getFilmController()
const dataValidator = getFilmMiddlewareDataValidator()
const voteController = getVoteController()

filmRouter.post(
    '/new/:userId',
    (req, res, next) => auth.checkToken(req, res, next),
    (req, res, next) => auth.checkUserPermission(req, res, next, userService, true),
    (req, res, next) => dataValidator.validateCreation(req, res, next),
    (req, res) => filmController.create(req, res)
)

filmRouter.get(
    '/all',
    (req, res, next) => auth.checkToken(req, res, next),
    (req, res, next) => filmController.getAll(req, res, next),
    (req, res) => voteController.getVotesFromFilms(req, res),
)


export { filmRouter }