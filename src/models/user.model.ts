import { Column, CreateDateColumn, Entity, ManyToMany, OneToMany, UpdateDateColumn } from "typeorm";
import { Vote } from "./vote.model";

@Entity()
export class User {
    @Column({ readonly: true, primary: true, generated: 'uuid', type: 'uuid' })
    public id: string
    @Column()
    public name: string
    @Column({ unique: true })
    public email: string
    @Column()
    public password: string
    @Column({ readonly: true })
    public role: string
    @Column({ name: 'is_active', default: true })
    public isActive: boolean
    @CreateDateColumn({ name: 'created_at', readonly: true })
    public createdAt: Date
    @UpdateDateColumn({ name: 'updated_at' })
    public updatedAt: Date
    @OneToMany(() => Vote, vote => vote.user)
    public votes: Vote[]
}