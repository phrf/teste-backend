import { Column, CreateDateColumn, Entity, OneToMany } from "typeorm";
import { Vote } from "./vote.model";

@Entity()
export class Film {
    @Column({ readonly: true, primary: true, generated: 'uuid', type: 'uuid' })
    public id: string
    @Column()
    public name: string
    @Column()
    public genre: string
    @Column()
    public director: string
    @Column('text', { array: true })
    public actors: string[]
    @CreateDateColumn({ name: 'created_at', readonly: true })
    public createdAt: Date
    @OneToMany(() => Vote, vote => vote.film)
    public votes: Vote[]
}