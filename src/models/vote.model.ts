import { Column, Entity, ManyToOne } from "typeorm";
import { Film } from "./film.model";
import { User } from "./user.model";

@Entity()
export class Vote {
    @Column({ readonly: true, primary: true, generated: 'uuid', type: 'uuid' })
    public id: string
    @Column()
    public rate: number
    @ManyToOne(() => User, user => user.votes)
    public user: User
    @ManyToOne(() => Film, film => film.votes)
    public film: Film

}