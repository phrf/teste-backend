import { app } from './express'
import 'reflect-metadata'
import { connectToDabase } from './database'
connectToDabase().then(_ => import('./routes'))
app.listen(process.env.SERVER_PORT, () => console.log(`Server running on http://${process.env.SERVER_HOST}:${process.env.SERVER_PORT}`))