import { createConnection, ConnectionOptions } from "typeorm"

export async function connectToDabase() {
    const connectionOptions: ConnectionOptions = {
        type: 'postgres',
        host: process.env.POSTGRES_HOST,
        port: Number(process.env.POSTGRES_PORT),
        username: process.env.POSTGRES_USERNAME,
        password: process.env.POSTGRES_PASSWORD,
        migrations: ['./database/migrations/*.ts'],
        entities: ['./src/models/*.ts'],
        cli: {
            migrationsDir: './src/database/migrations/'
        }
    }
    await createConnection(connectionOptions)
    console.log(`Database connected in host:${process.env.POSTGRES_HOST} and port:${process.env.POSTGRES_PORT}`)
}
