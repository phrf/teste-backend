import { EntityRepository, Repository } from "typeorm";
import { Film } from "../models/film.model";
@EntityRepository(Film)
export class FilmRepositorie extends Repository<Film>{ }