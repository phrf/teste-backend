import { EntityRepository, Repository } from "typeorm";
import { Vote } from "../models/vote.model";
@EntityRepository(Vote)
export class VoteRepositorie extends Repository<Vote>{ }