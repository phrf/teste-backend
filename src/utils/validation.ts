export function isValidEmailFormat(email: string) {
    if (!email) {
        return false
    }
    const emailRegex = new RegExp(/\@/)
    return !!email.match(emailRegex)
}

export function isValidaPasswordFormat(password: string) {
    const passwordRegex = new RegExp(/(?=.*\d)(?=.*\D).{8,}/g)
    return !!password.match(passwordRegex)
}

export function isNullOrEmpty(data: string | number) {
    return data === undefined ? true : !data
}

export function isArrayNullOrEmpyt(data: any[]) {
    if (data == undefined)
        return true
    else if (!data.length) {
        return true
    }
    else {
        let someItemIsNullOrEmpty = false
        for (const item of data) {
            if (item !== 0 && !item) {
                someItemIsNullOrEmpty = true
                break
            }
        }
        return someItemIsNullOrEmpty
    }

}