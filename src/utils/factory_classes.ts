import { FilmController } from "../controllers/film.controller";
import { UserController } from "../controllers/user.controller";
import { VoteController } from "../controllers/vote.controller";
import { AuthMiddleware } from "../middlewares/auth.middleware";
import { FilmMiddleware } from "../middlewares/film.middleware";
import { FilmMiddlewareDataValidator } from "../middlewares/validators/film.middleware";
import { UserMiddlewareDataValidator } from "../middlewares/validators/user.middleware";
import { AuthService } from "../services/auth.service";
import { FilmService } from "../services/film.service";
import { HashingService } from "../services/hashing.service";
import { UserService } from "../services/user.service";
import { VoteService } from "../services/vote.service";

export function getAuthMiddleware() {
    const authService = getAuthService()
    return new AuthMiddleware(authService)
}

export function getVoteService() {
    const filmService = getFilmService()
    const userService = getUserService()
    return new VoteService(filmService, userService)
}


export function getVoteController() {
    const voteService = getVoteService()
    return new VoteController(voteService)
}

export function getUserMiddlewareDataValidator() {
    return new UserMiddlewareDataValidator()
}

export function getFilmMiddlewareDataValidator() {
    return new FilmMiddlewareDataValidator()
}

export function getFilmMiddleware(){
    const filmService = getFilmService()
    return new FilmMiddleware(filmService)
}

export function getFilmService() {
    return new FilmService()
}

export function getFilmController() {
    const filmService = getFilmService()
    return new FilmController(filmService)
}

export function getAuthService() {
    return new AuthService()
}

export function getHashingService() {
    return new HashingService()
}

export function getUserService() {
    const hashingService = getHashingService()
    const authService = getAuthService()
    return new UserService(hashingService, authService)
}

export function getUserController(){
    const userService = getUserService()
    return new UserController(userService)
}
