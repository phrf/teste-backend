import { Request, Response } from 'express'
import { Film } from '../models/film.model'
import { VoteService } from '../services/vote.service'

export class VoteController {
    constructor(voteService: VoteService) {
        this.service = voteService
    }

    private service: VoteService

    async create(req: Request, res: Response) {
        try {
            const { body } = req
            const { filmId, userId } = req.params
            const vote = await this.service.create(body, filmId, userId)
            return res.status(201).send(vote)

        } catch (error) {
            return res.status(400).send('Erro')
        }
    }

    async getVotesFromFilms(req: Request, res: Response) {
        const films = res.locals.films as Film[]
        const filmsWithVotes = []
        for (const film of films) {
            let rate = 0;
            let rateMedia = 0;
            const [votes, numberOfVotes] = await this.service.getVoteInfoFromFilm(film)
            votes.forEach((vote) => {
                rate += vote.rate
            })
            if (numberOfVotes !== 0) {
                rateMedia = Number((rate / numberOfVotes).toFixed(2))
            }
            filmsWithVotes.push({ ...film, totalRate: rate, rateMedia: rateMedia })
        }
        res.status(200).send(filmsWithVotes)
    }

}