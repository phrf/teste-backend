import { NextFunction, Request, Response } from 'express'
import { UserService } from "../services/user.service"
import { FilmService } from "../services/film.service"

export class FilmController {
    constructor(filmService: FilmService) {
        this.service = filmService
    }

    private service: FilmService

    async create(req: Request, res: Response) {
        try {
            const { body } = req
            const film = await this.service.create(body)
            return res.status(201).send(film)
        } catch (error) {
            return res.status(500).send({
                'error': 'Internal error'
            })
        }
    }

    async getAll(req: Request, res: Response, next: NextFunction) {
        const { name, director, genre, actors } = req.query
        const dataToFilter: any = this.fillFilterData(name as string, director as string, genre as string, actors as string)
        let films = []
        if (!dataToFilter) {
            films = await this.service.getFilms()
        } else {
            films = await this.service.getFilms(dataToFilter)
        }
        res.locals.films = films
        return next()
    }


    private fillFilterData(name: string, director: string, genre: string, actors: string) {
        const data: any = {}
        if (name !== undefined)
            data['name'] = name
        if (director !== undefined)
            data['director'] = director
        if (genre !== undefined)
            data['genre'] = genre
        if (actors !== undefined)
            data['actors'] = actors
        return data
    }
}
