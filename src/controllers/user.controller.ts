import { Request, Response } from "express";
import { UserService } from "../services/user.service";

export class UserController {
    private service: UserService
    constructor(service: UserService) {
        this.service = service
    }

    async create(req: Request, res: Response) {
        const { body } = req
        const creationResult = await this.service.create(body)
        if (creationResult.containsError) {
            return res.status(400).send({
                error: creationResult.errorMessage
            })
        }
        return res.status(201).send(creationResult.user)
    }

    async edit(req: Request, res: Response) {
        const { id } = req.params
        const userExist = await this.service.checkUserExistance(id)
        if (userExist) {
            const { body } = req
            const editResult = await this.service.edit(id, body)
            if (editResult.containsError) {
                return res.status(400).send({ 'error': editResult.errorMessage })
            }
            return res.status(200).send(editResult.user)
        }
        else
            return res.status(422).send({
                'message': 'The id required not exist, check if is an valid uuid'
            })
    }

    async delete(req: Request, res: Response) {
        const { id } = req.params
        const userExist = await this.service.checkUserExistance(id)
        if (userExist) {
            await this.service.delete(id)
            res.status(204).send()
        }
        else
            return res.status(422).send({
                'message': 'The id required not exist, check if is an valid uuid'
            })
    }
}