import { getCustomRepository } from "typeorm";
import { User } from "../models/user.model";
import { UserRepositorie } from "../repositories/user.respositorie";
import { HashingService } from "./hashing.service";
import { AuthService } from './auth.service'


interface UserInOperation {
    id: string
    name: string
    email: string
    role: string
    token: string
}

interface UserOperationResult {
    user: UserInOperation | {}
    containsError: boolean
    errorMessage?: string
}

interface INewUser {
    name: string
    email: string
    password: string
    role: string
}

interface IEditUser {
    name: string
    email: string
}


export class UserService {
    constructor(hashingService: HashingService, authService: AuthService) {
        this.userRepositorie = getCustomRepository(UserRepositorie)
        this.encryptor = hashingService
        this.auth = authService
    }

    private userRepositorie: UserRepositorie
    private encryptor: HashingService
    private auth: AuthService

    async create(data: INewUser): Promise<UserOperationResult> {
        try {
            const { email, password } = data
            const emailAlreadyExist = await this.emailAlreadyExist(email)
            if (emailAlreadyExist) {
                return {
                    containsError: true,
                    errorMessage: 'An user with this email already exist',
                    user: {}
                }
            }
            data.password = await this.encryptor.hashPassword(password)
            const user = this.userRepositorie.create(data)
            return this.userRepositorie.save(user).then((user) => {
                return { containsError: false, user: this.returnUser(user) }
            })
        } catch (error) {
            return { containsError: true, user: {} }
        }
    }

    async edit(id: string, data: IEditUser): Promise<UserOperationResult> {
        const { name, email } = data
        const currentUser = await this.userRepositorie.findOne(id)
        if (currentUser!.email !== email) {
            const emailAlreadyExist = await this.emailAlreadyExist(email)
            if (emailAlreadyExist) {
                return {
                    containsError: true,
                    errorMessage: 'An user with this email already exist',
                    user: {}
                }
            }
        }
        const { affected } = await this.userRepositorie.update(id, {
            name: name,
            email: email
        })
        if (affected != undefined && affected > 0) {
            currentUser!.name = name
            currentUser!.email = email
            return { containsError: false, user: this.returnUser(currentUser!) }
        } else {
            return { containsError: true, user: {} }
        }
    }

    async getUser(id: string) {
        return await this.userRepositorie.findOne(id)
    }

    async delete(id: string) {
        await this.userRepositorie.update(id, {
            isActive: false
        })
    }

    async isUserAdmin(id: string) {
        try {
            const user = await this.userRepositorie.findOne(id)
            return user?.role === 'Admin'
        } catch (error) {
            return false
        }
    }

    async checkUserExistance(id: string) {
        try {
            const userCount = await this.userRepositorie.count({ where: { id: id } })
            if (userCount === 0)
                return false
            return true
        } catch (error) {
            return false
        }
    }

    private async emailAlreadyExist(email: string) {
        return await this.userRepositorie.count({
            where: {
                email: email
            }
        }) != 0
    }

    private createTokenPayload(name: string, email: string) {
        return {
            'name': name,
            'email': email
        }
    }

    private returnUser(user: User): UserInOperation {
        const token = this.auth.createToken(this.createTokenPayload(user.name, user.email))
        return {
            'id': user.id,
            'name': user.name,
            'email': user.email,
            'role': user.role,
            'token': token
        }
    }

}