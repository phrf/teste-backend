import { FindOperator, getCustomRepository, In } from "typeorm";
import { Film } from "../models/film.model";
import { FilmRepositorie } from "../repositories/film.repositorie";


interface INewFilm {
    name: string
    director: string
    genre: string
    actors: string[]
}

interface IGetFilmsFilter {
    name?: string
    genre?: string
    director?: string
    actors?: string | string[] | FindOperator<string>
}

export class FilmService {
    constructor() {
        this.filmRepositorie = getCustomRepository(FilmRepositorie)
    }

    private filmRepositorie: FilmRepositorie

    async create(data: INewFilm) {
        const film = this.filmRepositorie.create(data)
        await this.filmRepositorie.save(film)
        return film
    }

    async getFilm(id: string) {
        return await this.filmRepositorie.findOne(id)
    }

    async getFilms(data: IGetFilmsFilter | void) {
        if (data) {
            if (data.actors) {
                const formattedActors = this.formatActorsFilter(data.actors as string)
                data!.actors = In(formattedActors)
            }
            return await this.filmRepositorie.find({
                where: { ...data }
            })
        }
        return await this.filmRepositorie.find()
    }

    private formatActorsFilter(actors: string) {
        return [`{${actors}}`]
    }

    async checkFilmExistance(id: string) {
        try {
            const userCount = await this.filmRepositorie.count({ where: { id: id } })
            if (userCount === 0)
                return false
            return true
        } catch (error) {
            return false
        }
    }
}