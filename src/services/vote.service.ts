import { getCustomRepository } from "typeorm";
import { Film } from "../models/film.model";
import { User } from "../models/user.model";
import { Vote } from "../models/vote.model";
import { VoteRepositorie } from "../repositories/vote.repositorie";
import { FilmService } from "./film.service";
import { UserService } from "./user.service";


interface IVote {
    rate: number
    film?: Film
    user?: User
}

export class VoteService {
    constructor(filmService: FilmService, userService: UserService) {
        this.voteRepositorie = getCustomRepository(VoteRepositorie)
        this.userService = userService
        this.filmService = filmService
    }

    private voteRepositorie: VoteRepositorie
    private userService: UserService
    private filmService: FilmService


    async create(data: IVote, filmId: string, userId: string) {
        data.film = await this.filmService.getFilm(filmId)
        data.user = await this.userService.getUser(userId)
        const vote = this.voteRepositorie.create(data)
        await this.voteRepositorie.save(vote)
        return this.formatReturnVote(vote)
    }

    async getVoteInfoFromFilm(film: Film) {
        return await this.voteRepositorie.findAndCount({
            where: {
                film: film
            }
        })
    }

    formatReturnVote(vote: Vote) {
        return {
            'rate': vote.rate
        }
    }
}