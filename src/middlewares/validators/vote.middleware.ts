import { Request, Response, NextFunction } from "express";
import { isArrayNullOrEmpyt, isNullOrEmpty } from "../../utils/validation";


interface DataValidationError {
    field: string
    message: string
}

export class VoteMiddlewareDataValidator {
    private dataValidation: DataValidationError = {
        field: '',
        message: ''
    }

    validateCreation(req: Request, res: Response, next: NextFunction) {
        let { rate } = req.body
        rate = Number(rate)
        if (isNullOrEmpty(rate)) {
            this.dataValidation.field = 'rate'
            this.dataValidation.message = 'The rate should be in a interval between 0 and 4'
        }
        else if (rate > 4 || rate < 0) {
            this.dataValidation.field = 'rate'
            this.dataValidation.message = 'The rate should be in a interval between 0 and 4'
        } else {
            return next()
        }
        return res.status(400).send({ ...this.dataValidation })
    }

}