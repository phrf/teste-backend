import { isNullOrEmpty, isValidaPasswordFormat, isValidEmailFormat } from "../../utils/validation"
import { NextFunction, Request, Response } from 'express'

interface DataValidationError {
    field: string
    message: string
}

export class UserMiddlewareDataValidator {
    private dataValidation: DataValidationError = {
        field: '',
        message: ''
    }

    private isEmailValid(email: string) {
        return isValidEmailFormat(email)
    }

    private isRoleValid(role: string) {
        return role == 'Admin' || role == 'Common'
    }

    private isPasswordValid(password: string) {
        return isValidaPasswordFormat(password)
    }

    validateCreation(req: Request, res: Response, next: NextFunction) {
        const { name, email, password, role } = req.body
        if (isNullOrEmpty(name)) {
            this.dataValidation.field = 'name'
            this.dataValidation.message = 'The name field should\'t be null or empty'
        }
        else if (!this.isEmailValid(email)) {
            this.dataValidation.field = 'email'
            this.dataValidation.message = 'The email field should\'t be null,empty or not contains @'
        }
        else if (!this.isRoleValid(role)) {
            this.dataValidation.field = 'role'
            this.dataValidation.message = 'The role field should be equal to "Admin" or "Common"'
        } else {
            if (!this.isPasswordValid(password)) {
                this.dataValidation.field = 'password'
                this.dataValidation.message = 'The password should contains leters, numbers and have 8 charcacters'
            }
        }
        if (this.dataValidation.field) {
            return res.status(400).send({ ...this.dataValidation })
        }
        return next()
    }

    validateEdition(req: Request, res: Response, next: NextFunction) {
        const { name, email } = req.body
        if (isNullOrEmpty(name)) {
            this.dataValidation.field = 'name'
            this.dataValidation.message = 'The name field should\'t be null or empty'
        }
        else if (!this.isEmailValid(email)) {
            this.dataValidation.field = 'email'
            this.dataValidation.message = 'The email field should\'t be null,empty or not contains @'
        }
        if (this.dataValidation.field) {
            return res.send(400).send({ ...this.dataValidation })
        }
        return next()
    }
}