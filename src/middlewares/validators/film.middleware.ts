import { Request, Response, NextFunction } from "express";
import { isArrayNullOrEmpyt, isNullOrEmpty } from "../../utils/validation";


interface DataValidationError {
    field: string
    message: string
}

export class FilmMiddlewareDataValidator {
    private dataValidation: DataValidationError = {
        field: '',
        message: ''
    }

    validateCreation(req: Request, res: Response, next: NextFunction) {
        const { name, genre, director, actors } = req.body
        if (isNullOrEmpty(name)) {
            this.dataValidation.field = 'name'
            this.dataValidation.message = 'The name should\'t be empyty or null'

        } else if (isNullOrEmpty(genre)) {
            this.dataValidation.field = 'genre'
            this.dataValidation.message = 'The genre should\'t be empyty or null'
        }
        else if (isNullOrEmpty(director)) {
            this.dataValidation.field = 'director'
            this.dataValidation.message = 'The director should\'t be empyty or null'
        }
        else if (isArrayNullOrEmpyt(actors)) {
            this.dataValidation.field = 'actors'
            this.dataValidation.message = 'The actors should\'t be empyty or null, it should at leat contains 1 actor'
        }
        else {
            return next()
        }
        return res.status(400).send({ ...this.dataValidation })
    }

}