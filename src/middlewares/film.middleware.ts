import { NextFunction, Request, Response } from "express";
import { FilmService } from "../services/film.service";

export class FilmMiddleware {
    constructor(filmService: FilmService) {
        this.filmService = filmService
    }
    private filmService: FilmService

    async filmExists(req: Request, res: Response, next: NextFunction) {
        const { filmId } = req.params
        const filmExist = await this.filmService.checkFilmExistance(filmId)
        if (filmExist)
            return next()
        else
            res.status(400).send({ 'error': 'The film of this id does not exist' })
    }
}