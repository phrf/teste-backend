import { Request, Response, NextFunction } from 'express'
import { UserService } from '../services/user.service'
import { AuthService } from '../services/auth.service'

export class AuthMiddleware {
    constructor(authService: AuthService) {
        this.auth = authService
    }
    private auth: AuthService
    checkToken(req: Request, res: Response, next: NextFunction) {
        const authorization = req.header('Authorization')
        if (authorization == undefined || !authorization)
            return res.status(400).send({
                'error': 'Missing "Authorization" header'
            })
        if (!authorization.match('Bearer'))
            return res.status(400).send({
                'error': 'Missing "Bearer" in "Authorization" header'
            })
        const token = authorization.replace('Bearer', '').trim()
        if (!token)
            return res.status(400).send({
                'error': 'Missing token in "Authorization" header'
            })
        if (this.auth.isValidToken(token))
            return next()
        else
            return res.status(403).send({
                'error': 'Unathorizated'
            })
    }

    async checkUserPermission(req: Request, res: Response, next: NextFunction, userService: UserService, isToPermitAdmin: boolean) {
        const { userId } = req.params
        const userExist = await userService.checkUserExistance(userId)
        if (userExist) {
            const isUserAdmin = await userService.isUserAdmin(userId)
            if (isUserAdmin && isToPermitAdmin) {
                return next()
            } else {
                if (!isToPermitAdmin) {
                    return next()
                }
                return res.status(403).send({
                    'error': 'Unathorizated'
                })
            }
        }
        return res.status(400).send({
            'error': 'User id does not exist'
        })

    }

}